import csv
import os

from config import SOURCE_PATH, DL_PATH


""" -- InstallerSource

  Read sources to Wise installers.

  The file format is CSV with the following columns
  (b3, md5, filesize, url). It does not have a header.
"""

class InstallerSource:
  def __init__(self, data):
    self.b3, self.md5, self.filesize, self.dlUrl = data
    self.filesize = int(self.filesize)
    self.filepath = os.path.join(DL_PATH, self.b3)

  def getFilename(self):
    return os.path.basename(self.dlUrl)


def InstallerSourceIt(swFile):
  with open(swFile, newline='') as fp:
    for row in csv.reader(fp):
      yield InstallerSource(row)


def InstallerSourceUniqIt(swFile):
  sums = []
  with open(swFile, newline='') as fp:
    for row in csv.reader(fp):
      sw = InstallerSource(row)
      if sw.b3 in sums:
        continue
      sums.append(sw.b3)
      yield sw


def InstallerSourceAllUniqIt():
  for ent in os.listdir(SOURCE_PATH):
    abspath = os.path.join(SOURCE_PATH, ent)
    if not os.path.isfile(abspath):
      continue

    if os.path.splitext(ent)[1] != ".csv":
      continue

    for source in InstallerSourceUniqIt(abspath):
      yield source

