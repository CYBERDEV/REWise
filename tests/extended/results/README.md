# Results

These are the test results.


## `shareware.csv`

  > Use `python ../test.py print-stats shareware.csv` to print stats.

| DATE                | OK   | ERR  | LOK  | LERR | ROK  | RERR | v02  | v02E | VERSION
| :------------------ | ---: | ---: | ---: | ---: | ---: | ---: | ---: | ---: | :------
| NE                  |   96 |    6 |  100 |    2 |  101 |    1 |    0 |  102 | Unknown
| 1998-10-02 22:36:51 |   78 |    4 |   78 |    4 |   82 |    0 |   58 |   24 | Unknown
| 1998-10-23 23:23:16 |    2 |    0 |    2 |    0 |    2 |    0 |    2 |    0 | Unknown
| 1998-11-09 21:17:09 |  176 |   22 |  179 |   19 |  198 |    0 |  132 |   66 | InstallMaster 7
| 1998-12-03 23:11:32 |   43 |    0 |   43 |    0 |   43 |    0 |   28 |   15 | Unknown
| 1999-02-05 23:07:52 |  214 |   19 |  233 |    0 |  232 |    1 |  132 |  101 | Unknown
| 1999-04-05 18:07:26 |    4 |    0 |    4 |    0 |    4 |    0 |    4 |    0 | Unknown
| 1999-05-21 22:48:48 |  406 |   33 |  420 |   19 |  432 |    7 |  267 |  172 | Unknown
| 1999-08-17 17:25:48 |  238 |   24 |  250 |   12 |  254 |    8 |  143 |  119 | Unknown
| 2000-04-25 16:37:12 |  894 |   49 |  914 |   29 |  937 |    6 |  514 |  429 | InstallMaster 8
| 2001-08-13 19:13:38 |  319 |    8 |  325 |    2 |  323 |    4 |  222 |  105 | Unknown
| 2001-10-25 21:47:11 |  419 |   22 |  432 |    9 |  430 |   11 |  246 |  195 | Installation System 9
| 2031-05-20 22:54:01 |    4 |    0 |    4 |    0 |    4 |    0 |    4 |    0 | Unknown
| ------------------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | -------
| Total:         3080 | 2893 |  187 | 2984 |   96 | 3042 |   38 | 1752 | 1328 | 

```
PK Total    : 438
PK OK       : 432
PK ERROR    : 6
PK Raw OK   : 438
PK Raw ERROR: 0

Comments:
 80 Without comment
 21 Negative script deflate offset
 21 Possible weird stuff at WiseScriptHeader.unknown_22 '9190A09A878B'.
 14 Possible weird stuff at WiseScriptHeader.unknown_22 '958A928F8C8B'.
 10 Possible weird stuff at WiseScriptHeader.unknown_22 'B890BE889E86'.
  6 Possible weird stuff at WiseScriptHeader.unknown_22 'B6BF88BF96BF'.
  6 Possible weird stuff at WiseScriptHeader.unknown_22 'A8B0ADB3BBB6'.
  5 'Initializing Wise Installation Wizard...' found in the PE b'.data\x00\x00\x00' section.
  3 Possible weird stuff at WiseScriptHeader.unknown_22 'BCADA6CEA5CD'.
  3 Possible weird stuff at WiseScriptHeader.unknown_22 'C8CACBC8C6CC'.
  2 Possible weird stuff at WiseScriptHeader.unknown_22 '99939E9D919E'.
  2 0x18 mismatch, 0x18 00 08 ..
  2 Possible weird stuff at WiseScriptHeader.unknown_22 'DB8F9E8C8C88'.
  2 Possible weird stuff at WiseScriptHeader.unknown_22 'ADBF90BF93BF'.
  1 Possible weird stuff at WiseScriptHeader.unknown_22 '8588C9C6C88B'.
  1 Possible weird stuff at WiseScriptHeader.unknown_22 'AB969C949A8B'.
  1 0x18 mismatch, there is OP 0x00 right after 0x18
  1 Script deflate offset is absurd high
  1 Possible weird stuff at WiseScriptHeader.unknown_22 'CC918B90929D'.
  1 Possible weird stuff at WiseScriptHeader.unknown_22 'B4AAACB6B5AC'.
  1 Possible weird stuff at WiseScriptHeader.unknown_22 '9D8D9E919B96'.
  1 Possible weird stuff at WiseScriptHeader.unknown_22 '9C909B9A989A'.
  1 Possible weird stuff at WiseScriptHeader.unknown_22 '8C929E9393D1'.
  1 Overlay header unknown2 != 0x8000 but 0x0000.

Verify errors:
 75 WS_ERROR_VALUE     ERROR: parseWiseScriptHeader failed to read languageTexts.
 46 WS_ERROR_INFLATE   ERROR: Inflated size is larger then expected
 23 WS_ERROR_SEEK      ERROR: Failed seek to file offset
  9 ERROR_VALUE        ERROR: Advertised WiseScript.bin size looks insane.
  7 WS_ERROR_INFLATE   ERROR: zlib: invalid code lengths set
  4 WS_ERROR_INFLATE   ERROR: zlib: invalid distance too far back
  4 WS_ERROR_INFLATE   ERROR: zlib: invalid stored block lengths
  3 WS_ERROR_PK_HEADER ERROR: PK signature unknown
  3 WS_ERROR_VALUE     ERROR: wiseScriptParsePath expected '%' at path start
  2 WS_ERROR_VALUE     ERROR: parseWiseScript unknown OP: 3A
  2 WS_ERROR_INFLATE   ERROR: zlib: invalid block type
  2 ERROR_VALUE        ERROR: Advertised .dib size looks insane.
  1 WS_ERROR_BRANCHING ERROR: OP 0x23 LANG but not in LANG branch
  1 WS_ERROR_INFLATE   ERROR: zlib: invalid literal/length code
  1 WS_ERROR_EOF       ERROR: This is a multi language installer but not all languages have their name set
  1 WS_ERROR_VALUE     ERROR: parseWiseScript unknown OP: 49
  1 WS_ERROR_INFLATE   ERROR: zlib: invalid distance code
  1 WS_ERROR_BRANCHING ERROR: OP 0x23 LANG but currently not in a branch
  1 ERROR_INFLATE      ERROR: zlib: invalid stored block lengths
```

## `ignored.csv`

  > Use `python ../test.py print-stats ignored.csv` to print stats.

| DATE                | OK   | ERR  | LOK  | LERR | ROK  | RERR | v02  | v02E | VERSION
| :------------------ | ---: | ---: | ---: | ---: | ---: | ---: | ---: | ---: | :------
| NE                  |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 1992-06-20 00:22:17 |    0 |    9 |    0 |    9 |    0 |    9 |    0 |    9 | Unknown
| 1997-06-16 23:32:11 |    0 |   21 |    0 |   21 |    0 |   21 |    0 |   21 | Unknown
| 1999-02-05 23:07:52 |    0 |    2 |    2 |    0 |    0 |    2 |    0 |    2 | Unknown
| 1999-04-08 22:24:47 |    0 |  285 |    0 |  285 |    0 |  285 |    0 |  285 | Unknown
| 1999-04-17 21:22:36 |    0 |    3 |    0 |    3 |    0 |    3 |    0 |    3 | Unknown
| 1999-05-11 21:42:27 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 1999-05-21 22:48:48 |    0 |    2 |    2 |    0 |    0 |    2 |    0 |    2 | Unknown
| 1999-07-19 20:49:20 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 1999-08-16 11:00:06 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 1999-08-17 17:25:48 |    0 |    3 |    1 |    2 |    0 |    3 |    0 |    3 | Unknown
| 1999-12-03 12:59:21 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 2000-03-21 16:49:03 |    0 |    8 |    0 |    8 |    0 |    8 |    0 |    8 | Unknown
| 2000-04-19 20:27:55 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2000-04-25 16:37:12 |    0 |    2 |    1 |    1 |    0 |    2 |    0 |    2 | InstallMaster 8
| 2000-07-05 21:22:55 |    0 |    8 |    0 |    8 |    0 |    8 |    0 |    8 | Unknown
| 2000-08-22 19:05:09 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2000-09-25 22:23:56 |    0 |   11 |    0 |   11 |    0 |   11 |    0 |   11 | Unknown
| 2000-12-08 23:54:42 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2000-12-09 00:33:39 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2001-01-09 15:08:41 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2001-01-09 15:09:05 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2001-01-11 21:55:28 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2001-03-06 21:15:30 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2001-05-22 22:13:59 |    0 |   23 |    0 |   23 |    0 |   23 |    0 |   23 | Unknown
| 2001-08-10 01:13:34 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2001-08-13 19:13:38 |    0 |    1 |    1 |    0 |    0 |    1 |    0 |    1 | Unknown
| 2001-09-05 19:02:57 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2001-10-25 21:47:11 |    0 |    2 |    2 |    0 |    0 |    2 |    0 |    2 | Installation System 9
| 2001-10-26 14:34:09 |    0 |    3 |    0 |    3 |    0 |    3 |    0 |    3 | Unknown
| 2001-11-21 15:41:35 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 2001-12-19 16:19:42 |    0 |   10 |    0 |   10 |    0 |   10 |    0 |   10 | Unknown
| 2002-01-16 21:20:28 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2002-01-22 21:47:35 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 2002-02-12 16:33:09 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2002-03-29 17:59:03 |    0 |   12 |    0 |   12 |    0 |   12 |    0 |   12 | Unknown
| 2002-05-23 22:35:44 |    0 |   12 |    0 |   12 |    0 |   12 |    0 |   12 | Unknown
| 2002-06-24 20:22:31 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 2002-08-05 17:25:35 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2002-11-18 16:53:37 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2002-12-02 19:31:43 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 2002-12-16 20:16:07 |    0 |   17 |    0 |   17 |    0 |   17 |    0 |   17 | Unknown
| 2003-01-20 15:36:28 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2003-02-03 05:29:30 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2003-03-12 11:26:56 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2003-04-14 17:47:48 |    0 |    6 |    0 |    6 |    0 |    6 |    0 |    6 | Unknown
| 2003-04-22 18:23:04 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2003-04-24 15:10:23 |    0 |    4 |    0 |    4 |    0 |    4 |    0 |    4 | Unknown
| 2003-05-02 22:34:49 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2003-07-07 17:37:27 |    0 |   11 |    0 |   11 |    0 |   11 |    0 |   11 | Unknown
| 2003-10-09 21:05:12 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2003-11-21 19:16:37 |    0 |   23 |    0 |   23 |    0 |   23 |    0 |   23 | Unknown
| 2003-12-09 07:17:05 |    0 |    3 |    0 |    3 |    0 |    3 |    0 |    3 | Unknown
| 2004-01-29 07:13:04 |    0 |   20 |    0 |   20 |    0 |   20 |    0 |   20 | Unknown
| 2004-04-19 06:44:28 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2004-06-10 17:30:49 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2004-07-16 08:08:27 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 2004-08-12 15:48:03 |    0 |    9 |    0 |    9 |    0 |    9 |    0 |    9 | Unknown
| 2004-09-10 00:10:25 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2004-09-10 17:38:04 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 2004-10-22 09:20:02 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 2004-10-28 13:15:26 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2004-12-10 17:05:37 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2005-04-08 16:46:17 |    0 |    7 |    0 |    7 |    0 |    7 |    0 |    7 | Unknown
| 2005-08-16 01:42:29 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2005-12-13 23:12:15 |    0 |   40 |    0 |   40 |    0 |   40 |    0 |   40 | Unknown
| 2006-11-13 19:32:07 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 2006-11-27 18:36:03 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2006-12-15 22:21:24 |    0 |    4 |    0 |    4 |    0 |    4 |    0 |    4 | Unknown
| 2007-01-20 06:07:04 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2007-01-29 21:17:00 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2007-03-26 16:49:41 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2007-08-16 20:13:16 |    0 |    8 |    0 |    8 |    0 |    8 |    0 |    8 | Unknown
| 2007-11-20 22:12:45 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 2007-11-20 22:52:34 |    0 |    9 |    0 |    9 |    0 |    9 |    0 |    9 | Unknown
| 2008-06-25 11:42:41 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2009-10-27 17:10:03 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2010-03-10 00:20:10 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2011-07-04 12:17:32 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 2012-02-17 00:42:19 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2012-03-20 15:00:47 |    0 |    2 |    0 |    2 |    0 |    2 |    0 |    2 | Unknown
| 2012-10-29 18:13:37 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2012-12-07 18:34:27 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| 2013-03-28 11:38:55 |    0 |    1 |    0 |    1 |    0 |    1 |    0 |    1 | Unknown
| ------------------- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | ---- | -------
| Total:          644 |    0 |  644 |    9 |  635 |    0 |  644 |    0 |  644 | 

```
PK Total    : 152
PK OK       : 0
PK ERROR    : 152
PK Raw OK   : 0
PK Raw ERROR: 152

Comments:
 10 Without comment
329 'Initializing Wise Installation Wizard...' found in the PE b'.rsrc\x00\x00\x00' section.
181 'Initializing Wise Installation Wizard...' found in the PE b'.rdata\x00\x00' section.
 41 No overlay data
 16 Overlay header unknown2 != 0x8000 but 0x0000.
 10 'Initializing Wise Installation Wizard...' found in the PE b'.WISE\x00\x00\x00' section.
  4 'Initializing Wise Installation Wizard...' found in the PE b'_winzip_' section.
  3 Overlay header unknown2 != 0x8000 but 0x3137.
  3 Overlay header unknown2 != 0x8000 but 0x496E.
  2 Overlay header unknown2 != 0x8000 but 0x481C.
  2 Overlay header unknown2 != 0x8000 but 0x5468.
  2 Overlay header unknown2 != 0x8000 but 0x6973.
  2 Overlay header unknown2 != 0x8000 but 0x003E.
  2 Overlay header unknown2 != 0x8000 but 0x8620.
  2 Possible weird stuff at WiseScriptHeader.unknown_22 '958A928F8C8B'.
  2 Overlay header unknown2 != 0x8000 but 0x312C.
  1 This not a PE file: 'DOS Header magic not found.'
  1 Overlay header unknown2 != 0x8000 but 0x383E.
  1 Overlay header unknown2 != 0x8000 but 0x6233.
  1 Overlay header unknown2 != 0x8000 but 0x7376.
  1 Overlay header unknown2 != 0x8000 but 0x3335.
  1 Overlay header unknown2 != 0x8000 but 0x0205.
  1 Overlay header unknown2 != 0x8000 but 0x0355.
  1 Overlay header unknown2 != 0x8000 but 0x2E31.
  1 Very small overlay size of 3496 bytes
  1 Overlay header unknown2 != 0x8000 but 0xE819.
  1 Overlay header unknown2 != 0x8000 but 0x0713.
  1 Overlay header unknown2 != 0x8000 but 0x4C49.
  1 Overlay header unknown2 != 0x8000 but 0x0062.
  1 Overlay header unknown2 != 0x8000 but 0xDF64.
  1 False positive PK, check the overlay header
  1 Overlay header unknown2 != 0x8000 but 0x7072.
  1 Overlay header unknown2 != 0x8000 but 0x6974.
  1 Overlay header unknown2 != 0x8000 but 0x6520.
  1 Overlay header unknown2 != 0x8000 but 0x7365.
  1 Overlay header unknown2 != 0x8000 but 0x0700.
  1 Overlay header unknown2 != 0x8000 but 0x74CD.
  1 Overlay header unknown2 != 0x8000 but 0x0787.
  1 Overlay header unknown2 != 0x8000 but 0x1100.
  1 Overlay header unknown2 != 0x8000 but 0xB611.
  1 Overlay header unknown2 != 0x8000 but 0x7E20.
  1 Overlay header unknown2 != 0x8000 but 0x4544.
  1 Overlay header unknown2 != 0x8000 but 0xA21E.
  1 Overlay header unknown2 != 0x8000 but 0x6173.
  1 Overlay header unknown2 != 0x8000 but 0x0AAF.
  1 Overlay header unknown2 != 0x8000 but 0x726F.
  1 Overlay header unknown2 != 0x8000 but 0xD753.
  1 Overlay header unknown2 != 0x8000 but 0xE50A.
  1 Overlay header unknown2 != 0x8000 but 0x6F67.

Verify errors:
354 ERROR_VALUE        ERROR: Advertised WiseScript.bin size looks insane.
227 ERROR_INVALID_PENE ERROR: analyzeExe overlay size to small.
 28 ERROR_VALUE        ERROR: Advertised .dib size looks insane.
 15 ERROR_INFLATE      ERROR: zlib: invalid stored block lengths
  4 WS_ERROR_INFLATE   ERROR: Inflated size is larger then expected
  4 WS_ERROR_CRC32     ERROR: CRC32 mismatch!
  2 ERROR_INFLATE      ERROR: zlib: invalid distance too far back
  2 WS_ERROR_VALUE     ERROR: parseWiseScriptHeader failed to read languageTexts.
  2 ERROR_INFLATE      ERROR: zlib: invalid block type
  1 ERROR_INVALID_PENE ERROR: analyzeExe this is not a PE file for sure. The MS-DOS header signature doesn't match (not MZ).
  1 ERROR_INVALID_PENE ERROR: analyzeExe overlay offset larger then file size.
  1 WS_ERROR_SYSTEM_IO ERROR: Failed to find PK central directory size
  1 WS_ERROR_CRC32     ERROR: PK CRC32 mismatch for '(null)'
  1 ERROR_INFLATE      ERROR: zlib: invalid code lengths set
  1 ERROR_INFLATE      ERROR: zlib: invalid distance code
```
