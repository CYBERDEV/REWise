# Analyze errors
import os
import pefile
import zipfile


def addCustomComments(installer):
  if installer.md5 == "32aad22244ee0c8e7dcd3f86dea9e347": # NPFG10.EXE
    installer.addComment("This one has '../' in one of it's filenames, it is resolved by escaping the '../' with '\\.\\./'")

  elif installer.md5 == "a91a9034e99da6917ed6ea4ae1017474": # b65full.exe
    installer.addComment("Corrupt, missing data, see "
                         "c3d819097d06d110f1b01111a89b095c for valid "
                         "data.")

  elif installer.md5 in ["70a6bbf41b7e9de48e711dbc26eac88b",  # dap43.exe
             "f463a1d47ac919160fc450d322c6c63e",  # dap4.exe
             "7d093e7e91b17b0b6ca427c91202c839"]: # dap3908.exe
    installer.addComment("0x18 mismatch, there is OP 0x00 right after 0x18")

  elif installer.md5 in ["5516bfdc0346f475300f83ebc6c76547", # lw3drv.exe
             "3be4f8d1fc6ede81d954b6914530dd2b", # lw3w2k.exe
             "1588bc310e9a71389f1069af159203ee", # lw3wme.exe
             "073c6e77547bcda5ce7b5ebe96f3eb01"]:# SBL51.EXE
    installer.addComment("0x18 mismatch")

  elif installer.md5 in ["7c763e4af5aca8cc2fabcc846b788692",  # DSiegeUpdate1_09_2-1_1_1460.exe
             "acccf3d47143f0cf2b6b893765586eb6",  # Dsiegeupdate1.0-1.11.1462_english.exe
             "3465e2b1eec01cc26b898c4ef3ef090e"]: # dsupdate1.1-1.11.1462_eng.exe
    installer.addComment("0x18 mismatch, 0x18 00 08 ..")

  #elif installer.md5 in ["2b4e75b6c5b1ae0fd9243d0cf8db45e5" # psp700ev.exe
  #          "215506a69c9677e9b01bd5c43314ee5f"]: # RD2_onlinedemo_03_10.exe
  # installer.addComment("False positive, this is a InstallShield installer")

  elif installer.md5 in ["dbbf3189c879ad7673d5971e457effeb",  # DPPopUpStopper.exe
             "ae59de2259f3a109a6d66eb037da2335",  # ICQ2000B.EXE
             "60aef2e657c05cfff2bfbc75d6385f3d"]: # DontPanc.exe
    installer.addComment("This installer reads the overlay header 3 bytes to early")

  #elif installer.md5 in ["a44f7489825a35b8ea49857d2d832ce6",  # thmpls450_s.exe
  #          "cf8e337b9c331cdfbe490a023b184a9f",  # icq99b.exe
  #          "058f7c9124cd32463e8dfebe4eb115c7"]: # icq2000a.exe
  # installer.addComment("FIXME Weird extra chars in unknown_22")

  elif installer.md5 in ["3379f7b191a5f9ad9dee9d49acf99e23",  # upgrade.exe
           "3319738353934531293f8b7e1c103a85"]: # upgrade.exe
    installer.addComment("Interesting one, this has no 0x00 files, only 0x12, online installer?")

  elif installer.md5 == "28b29549010d0980d1cd1d0ad86f8dbe": # battlefield2demo.exe
    installer.addComment("False positive, InstallShield Wizard?")

  elif installer.md5 in ["32a6454ef70526f8d80be1069749bfde",  # sumofree.exe  (overlay: 0x00012200)
             "3dd7c98df5e6a5e528d446f4ebce46c8",  # RD210.EXE (overlay: 0x00012C00)
             "3c50c1616008c582c7ea3c34fef90d91"]: # empireearth2_mpdemo_en.exe  (overlay: 0x0001F800)
    installer.addComment("False positive? Raw files appended at overlay with probably a Wise installer")

  elif installer.md5 in ["7c420b5be50635f3a2f73cf8e5c490c5",  # spf.exe
                         "99c37f56b1a030ed4b04e0b2cb611051",  # spf.exe
                         "c087ee0bbd1baf2a6e04151b76fc1216"]: # spf.exe
    installer.addComment("False positive")


def addPeComments(installer):
  pe = None
  try:
    pe = pefile.PE(installer.realpath, fast_load=True)
  except pefile.PEFormatError as err:
    installer.addComment(f"This not a PE file: {err}")
    return True

  # find the magic string in any PE section and note the offset after read
  magicoffset = 0
  sectionName = ""
  with open(installer.realpath, "rb") as fp:
    for section in pe.sections:
      if section.Name[:5] == b'.WISE':
        installer.addComment("PE .WISE section found.")

    # Read each section into mem and search for the magic string
    for section in pe.sections:
      fp.seek(section.VirtualAddress, 0)
      sectiondata = fp.read(section.SizeOfRawData)
      if b"Initializing Wise Installation Wizard..." in sectiondata:
        magicoffset = fp.tell()
        sectionName = section.Name
        break

  if magicoffset != 0:
    installer.addComment(f"'Initializing Wise Installation Wizard...' found in the PE {sectionName} section.")
    pe.close()
    return True

  overlay = pe.get_overlay_data_start_offset()
  if overlay:
    filesize = os.path.getsize(installer.realpath)
    delta = filesize - overlay
    if delta < 4096:
      installer.addComment(f"Very small overlay size of {delta} bytes")
      pe.close()
      return True
  else:
    installer.addComment(f"No overlay data")
    pe.close()
    return True

    with open(installer.realpath, "rb") as fp:
      fp.seek(overlay, 0)
      if fp.read(13) == b'InstallShield':
        installer.addComment("False positive, 'InstallShield' found at overlay.")
        pe.close()
        return True

  pe.close()
  return False


def falsePositivePkCheck(installer):
  try:
    zfp = zipfile.ZipFile(installer.realpath, 'r')
    zfp.close()
  except zipfile.BadZipFile:
    installer.isPk = False
    installer.addComment("False positive PK, check the overlay header")
    return True
  return False

