# sources

`.exe` files from <http://cd.textfiles.com>,
<http://discmaster.textfiles.com> and <https://archive.org> have been
grepped for the `Initializing Wise Installation Wizard...` string. The
files here are the results.

There are duplicates since every URL found is listed.

All the `.csv` files here have the same `b3, md5, filesize, dl-url`
structure, without header.

## `shareware.csv`

 - Total size is 69.49 GiB


## `ignored.csv`

 - Total size is 17.08 GiB

These have been filtered out, they are assumed false positives.

See the `acceptableInstallerIt()` function in `../installers.py` for the
filter rules.


