CC = gcc
CFLAGS := -lz
CFLAGS += -Wall -fanalyzer

OBJECTS = src/rewise.o \
		  src/CP1252.o \
		  src/print.o \
		  src/reader.o \
		  src/exefile.o \
		  src/pkzip.o \
		  src/wildcard.o \
		  src/wisescript.o \
		  src/wiseoverlay.o



all: rewise

rewise: $(OBJECTS)
	$(CC) -o $@ -I./src $(CFLAGS) $^

debug: CFLAGS += -g -DREWISE_DEBUG
debug: rewise

# This builds REWise normal (no debug build) but unknown arguments
# like --debug will be allowed. This to make running tests easier
# between normal and debug build.
debug-dummy: CFLAGS += -DREWISE_DEBUG_DUMMY
debug-dummy: rewise

clean:
	rm -f src/*.o rewise

# Make sure header files are evaluated against changes
$(OBJECTS): src/CP1252.h \
            src/print.h \
            src/reader.h \
            src/exefile.h \
            src/pkzip.h \
            src/wisescript.h \
            src/wiseoverlay.h \
            src/version.h \
            src/errors.h \
            src/wildcard.h

.PHONY: all debug debug-dummy clean


